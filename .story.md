### EngX Design Patterns Dependency Injection Story

Welcome to a refactoring story about a small software component for SwiftShiftSend company.
This story illustrates how Dependency Injection Pattern offers a way to develop loosely coupled
code.

SwiftShiftSend started as a company centered around an innovative CRM system. Initially, it offered
SMS notifications to its users and relied on a PostgreSQL database for storing order information.