package com.epam.engx.story.orders;

public class OrderService {

  private final SmsNotifierService notifier = new SmsNotifierService();
  private final PostgresSqlOrderRepository orderRepository = new PostgresSqlOrderRepository();

  public void process() {
    Order order = new Order();
    // some logic related to order creations and filling
    orderRepository.saveOrder(order);

    notifier.sendSms("Order created");
  }
}
